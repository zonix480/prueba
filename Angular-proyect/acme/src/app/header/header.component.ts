import { Component, OnInit, Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  @Output() newItemEvent = new EventEmitter<string>();
  activeTab: string="Vehiculos";
  isCollapsed = true;
  constructor() { }
  ngOnInit(): void {
  }
  changeActive(ev: any) {
    console.log("change",ev)
    this.activeTab = ev;
    this.addNewItem(ev);
  }
  addNewItem(value: string) {
    this.newItemEvent.emit(value);
  }
}
