import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from './api.service';
import { AnimationItem } from 'lottie-web';
import { AnimationOptions } from 'ngx-lottie';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  cars: any = [];
  carsAll: any = [];
  persons: any = [];
  personsAll: any = [];
  options: AnimationOptions = {
    path: '/assets/car.json',
  };
  options2: AnimationOptions = {
    path: '/assets/person.json',
  };
  title = 'acme';
  active: any = 'Vehiculos';
  closeResult = '';
  changeCount = 0;
  searchText = "";
  activeCar = {
    "vehiculo": {
      "placa": "",
      "tipo": "",
      "color": "",
      "marca": ""
    },
    "dueno": {
      "primer_nombre": "",
      "segundo_nombre": "",
      "apellidos": "",
      "direccion": "3",
      "telefono": "",
      "cedula": ""
    },
    "conductor": {
      "primer_nombre": "",
      "segundo_nombre": "",
      "apellidos": "",
      "direccion": "",
      "telefono": "",
      "cedula": ""
    }
  }
  placaNew: any;
  colorNew: any;
  tipoNew: any;
  activePerson = {
    "cedula": "365834",
    "email": "dschmeler@yahoo.com",
    "primer_nombre": "Allen",
    "ciudad": "",
    "rol": "",
    "placa": "",
    "tipo": "",
    "segundo_nombre": "Raynor",
    "apellidos": "Bode",
    "direccion": "1646 Ramiro Island Apt. 429\nChamplintown, OK 71045",
    "telefono": "(870) 937-1664 x39086",
  }
  marcas: any = [];
  marcaNew: any;
  crearVehiculo: any;
  crearPersona: any;
  cedulaNew:any;
  emailNew:any;
  primer_nombreNew:any;
  segundo_nombreNew:any;
  apellidosNew:any;
  direccionNew:any;
  telefonoNew:any;
  rols_idNew:any;
  ciudad_idNew:any;
  cities:any = [];
  vehiculoNew:any;
  constructor(private modalService: NgbModal, public api: ApiService, private formBuilder: FormBuilder,) {
    //Setup form
    this.crearVehiculo = formBuilder.group({
      placa: ['', Validators.compose([Validators.required])],
      color: ['', Validators.compose([Validators.required])],
      tipo: ['', Validators.compose([Validators.required])],
      marca: ['', Validators.compose([Validators.required])],
    });
    this.crearPersona = formBuilder.group({
      cedula: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      primer_nombre: ['', Validators.compose([Validators.required])],
      segundo_nombre: ['', Validators.compose([Validators.required])],
      apellidos: ['', Validators.compose([Validators.required])],
      direccion: ['', Validators.compose([Validators.required])],
      telefono: ['', Validators.compose([Validators.required])],
      rols_id: ['', Validators.compose([Validators.required])],
      ciudad_id: ['', Validators.compose([Validators.required])],
      vehiculo: ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.loadData()
  }

  reloadVar(){
    this.marcaNew="";
    this.cedulaNew = ""
    this.emailNew = ""
    this.primer_nombreNew = ""
    this.segundo_nombreNew = ""
    this.apellidosNew = ""
    this.direccionNew = ""
    this.telefonoNew = ""
    this.rols_idNew = ""
    this.ciudad_idNew = ""
    this.vehiculoNew = ""
  }
  loadData() {
    this.cars = [];
    this.carsAll = [];
    this.persons = [];
    this.personsAll = [];
    this.api.getHeaders("vehiculos").subscribe((response: any) => {
      response.data.forEach((data: any) => {
        this.cars.push({
          "dueno": data.dueño[0],
          "conductor": data.conductor[0],
          "vehiculo": data.vehiculo
        })
        this.carsAll.push({
          "dueno": data.dueño[0],
          "conductor": data.conductor[0],
          "vehiculo": data.vehiculo
        })
      });
    })
    this.api.getHeaders("personas").subscribe((response: any) => {
      this.persons = response.data;
      this.personsAll = response.data;
    })
    this.api.getHeaders("marcas").subscribe((response: any) => {
      this.marcas = response.data;
    })
    this.api.getHeaders("ciudades").subscribe((response: any) => {
      this.cities = response.data;
    })
  }



  changeActive(ev: any) {
    this.active = ev;
    this.loadData();
  }


  crearVehiculoFunction() {
    if (this.crearVehiculo.valid) {
      this.api.postHeaders("vehiculos", {
        "placa": this.placaNew,
        "color": this.colorNew,
        "tipo": this.tipoNew,
        "marcas_id": this.marcaNew
      }).subscribe(response => {
        if(response){
          this.reloadVar()
          this.modalService.dismissAll()
          alert("Bien vehiculo creado correctamente");
        }
      },error=>{
        alert(error.error.message);
        this.modalService.dismissAll()
      })
    }else{
      alert("Revisa el formulario porfavor")
    }
  }

  crearPersonaFunction() {
    if (this.crearPersona.valid) {
      this.api.postHeaders("personas", {
        'cedula' : this.cedulaNew,
        'primer_nombre' :this.primer_nombreNew,
        'segundo_nombre': this.segundo_nombreNew,
        'apellidos': this.apellidosNew,
        'direccion' : this.direccionNew,
        'email' : this.emailNew,
        'telefono': this.telefonoNew,
        'ciudad_id' : this.ciudad_idNew,
        'rols_id' : this.rols_idNew,
        'vehiculo':this.vehiculoNew
      }).subscribe(
        response => {
          if(response.success){
            this.reloadVar()
            alert("Bien persona creada correctamente");
            this.modalService.dismissAll()
          }else{
            alert(response.message);
          }
        },
        error => {
          alert(error.error.message);
          this.modalService.dismissAll()
        })
    }else{
      console.log(this.crearPersona)
      alert("Revisa el formulario porfavor")
    }
  }

    applyFilter(event: any) {
      this.searchText = event.target.value;
      if (event.target.value == "") {
        this.loadData();
      } else {
        this.cars = this.carsAll.filter((val: any) => {
          return val.vehiculo.placa.toLowerCase().indexOf(event.target.value) > -1;
        });
        this.persons = this.personsAll.filter((val: any) => {
          return val.primer_nombre.toLowerCase().indexOf(event.target.value) > -1;
        });
      }
    }


    open(content: any, active: any,type:any) {
      if(active && !active.dueno && type == "Vehiculos"){
        alert("No tiene dueño este vehiculo")
      }else if(active && active.dueno  && type == "Vehiculos"){
        this.activeCar = active;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
      }
      if(type == "Personas"){
        this.activePerson = active;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
      }
      if(type == "VehiculosAbrir"){
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
      }
     
    }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
