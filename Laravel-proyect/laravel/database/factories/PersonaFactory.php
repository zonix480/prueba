<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Persona;
use Faker\Generator as Faker;

$factory->define(Persona::class, function (Faker $faker) {

    return [
        'cedula' => $faker->word,
        'primer_nombre' => $faker->word,
        'segundo_nombre' => $faker->word,
        'apellidos' => $faker->word,
        'direccion' => $faker->word,
        'telefono' => $faker->word,
        'ciudad_id' => $faker->randomDigitNotNull,
        'remember_token' => $faker->word,
        'rols_id' => $faker->randomDigitNotNull,
        'contraseña' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
