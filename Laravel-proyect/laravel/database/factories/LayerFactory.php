<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Layer;
use Faker\Generator as Faker;

$factory->define(Layer::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'image' => $faker->text,
        'designs_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
