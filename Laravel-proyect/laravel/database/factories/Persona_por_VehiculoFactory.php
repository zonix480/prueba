<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Persona_por_Vehiculo;
use Faker\Generator as Faker;

$factory->define(Persona_por_Vehiculo::class, function (Faker $faker) {

    return [
        'vehiculos_id' => $faker->randomDigitNotNull,
        'personas_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
