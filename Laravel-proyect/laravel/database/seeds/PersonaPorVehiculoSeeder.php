<?php

use Illuminate\Database\Seeder;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
class PersonaPorVehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=1;$i<=5;$i++){
            DB::table('persona_por_vehiculo')->insert([
                'vehiculos_id'      => $i,
                'personas_id'  =>  $i,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ]);
            DB::table('persona_por_vehiculo')->insert([
                'vehiculos_id'      => $i,
                'personas_id'  =>  $i+5,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ]);
        }
    }
}
