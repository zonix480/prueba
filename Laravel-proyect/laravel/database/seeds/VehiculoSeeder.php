<?php

use Illuminate\Database\Seeder;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
class VehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=0;$i<5;$i++){
            DB::table('vehiculos')->insert([
                'placa'      =>$faker->regexify('[A-Za-z0-9]{6}'),
                'color'  =>  $faker->colorName(),
                'tipo'  =>  $faker->randomElement(["particular","publico"]),
                'marcas_id'  =>  $faker->numberBetween(1,9),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ]);
        }
    }
}
