<?php

use Illuminate\Database\Seeder;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
class CiudadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=0;$i<20;$i++){
        // Create record 
        DB::table('ciudades')->insert([
            'nombre'      => $faker->city,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);
        }
    }
}
