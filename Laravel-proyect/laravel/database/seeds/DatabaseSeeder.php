<?php

use App\Models\Vehiculo;
use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Empty tables before execute seeds 
        $this->truncateTables([
            'ciudades',
            'marcas',
            'persona_por_vehiculo',
            'personas',
            'rols',
            'ciudades'
        ]);

        // Seeds 
        $this->call(CiudadSeeder::class);
        $this->call(MarcaSeeder::class);
        $this->call(PersonaPorVehiculoSeeder::class);
        $this->call(PersonasSeeder::class);
        $this->call(RolSeeder::class);
        $this->call(VehiculoSeeder::class);
    }

    // Drop content tables 
    public function truncateTables(array $tables){
        // Disabling foreign keys 
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        // Empty tables
        foreach($tables as $table){
            DB::table($table)->truncate();
        }
        // Enabling foreign keys 
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}