<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Effort Play</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/skins/_all-skins.min.css">

    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

    @yield('css')
</head>

<body class="skin-blue sidebar-mini">
@if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="#" class="logo">
                <b>Effort Play</b>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHEhUSBwgVFRAWFxoaGRMXGRoWGxkWFxcWGBYeHyEZHyogHRolHRkVIjEhJSorLi4uFx81ODUsNygtLisBCgoKDg0OFRAQFysdHR0tLS0tLSstLS0tLS0tLS0tLS0tLS0rLS0tLS0tNy0tLS0tLS0tLSstNzctLS03LSstN//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABwgEBQYCAQP/xABPEAACAQMCAgIMCAkICwAAAAAAAQIDBBEFBhIhBzETFiJBUVZhcYGRk9EUFzJSVZKh0xUjM0JUgoOxsiQlNGJjcrPSCERFRlNzlKKjwdT/xAAXAQEBAQEAAAAAAAAAAAAAAAAAAQID/8QAHBEBAQACAwEBAAAAAAAAAAAAAAEREgIhUUFh/9oADAMBAAIRAxEAPwCcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD43gDE1PU7LSoOpqV1ClTXXOclFZfUsvvmn7fdpeMdt7WPvIh/0h9wq7uaVlQn3NGPHUX9pUXcrzqHP9oRdpFnbXs3G81GFCCTfHOM5JtY5Ygm8v1cjc45iLX9vu0vGO29rH3jt92l4x23tY+8rT2u6H442/srj7sdr2ieONv7K4+7GsMrQadurQtVk4aXqtGrKMeKShNSxFNJt46llrn5TFe/NpxbUtw23L+1j7yFfgtvs3RK1Wzv41quoTVGnVhGUMUYOXZPlpPvTWcfnIjG2pdnlGLqRjxSS4pPEY5eMtrqS62JxFuO33aXjHbe1j7x2+7S8Y7b2sfeVt7TLfxx0z2tb7k+9pluv98dM9rW+5Jieiz1luXRtQpzq2WqUZ0qfy6kZpxh3+b6kYfb7tLxjtvax95DG97eOy9GttNjWhKvdTdetOGcSgmnDHEk8fkkm0vkMjLTbRX9WNOV1TpKWfxlVuMI4TeZNJtdWOrvos4wWz7fdpeMdt7WPvHb7tLxjtvax95W7tMoeOOme1rfcnqlsinVajS3dprk2kkqtZtt9SX4nrJieizlHcmkVqLuKWpUnbptOtxpQyutcT5Z8hhLfm0/GK19rD3kA9KdSGk/BtJtaicLOknUxnErmr3c35eTWPBxM53aO1NT3dWdDSVDjjBzcptxioppc2k+bbXIusFoe3zafjHa+1h7x2+bT8Y7X2sPeQZPoR3XTTc61qkllt1JYSXX+YRrJY6nnyjWX6q5ul69pmrpy0u/pVYx63TmpY8+Ooxe3LbXf1629rD3kCbInU2zpV/qKlw1K2LWh3suTzUkvDhNteWmyP9Ps62oVYUbWOalScYRX9aTSXo5jVFu+3LbP09be1h7x25bZ+nrb2sPeQmugnX+/qlr/AOX/ACEcbg0qWiXFS3qXEKkqUuFzhnhcklnGUnyfL0MawW90/cGkam+HTtTo1ZfNhUjJ48yeTGv93bd02pKlqGtUKdWOOKE6kYyWUmspvlyafpIN/wBH/QZX99K6lHubaDw+86lVSgl5e54/Wjotc6E9U1m4q3N1uGnx1Zym12KTxl8ku66ksJeYlklEk9vm0/GO19rD3jt82n4x2vtYe8qbq1rCwrVaNKspqnOUOyJYUuGTjxJZfJ45HZ9H/Rhe71ozr07+NGnGfAuKDlxNJOWMNcllL1l1mBYGnvja9VqNHcFtKUmkoqrFttvCSS7+ToIviIe2j0LVNDvKNze6rCrClLi7GoOLcknwPLb6pYfoJhisGbhX0AEAxtSuqNlSnVuZ4p04ucn4IxWX9iMk1+v6Tb69b1ba7nJU6seGTg8SSfgbTXrTAp/uDVaut3Na5uH3VWblh95N9yvQsL0GVp9nt2rTT1DW61Oq85hC2VVLm8d060c8sPqXWTY+gbbr/wBp3frp/dj4hdu/Sd166f3Z02iIb/B20vGS5/6KP/0mTpugba1OrTo2e4LmVSpOMIr4FH5Unhf6zyXlJb+IXbv0ndeun92bbavRJom2bmF1a3VedSClwqo4OKcouOe5iuaTZNhEPTDf0fhVOxsH/J7GlGjHyzwnN8v1V54sx+j3o6vt7xqzt7yNKFJxjxSi5cUpJtpYfeWPrIlW86D9Cvak6txqt25zlKUnmnzlJuTf5PwtncbN2zZ7St1bafOcoKUpOU8cTlJ5beEl4F1dSQ266EQfEDqP09S9nL3mRp3QPc29anO81inOlGcXOCpyTlBSTlHm8c1leknQx76g7qnOEasoOcZR4444o8SaysprKzlZT6ibUVU6Ute/D+pVqlKWaVN9ip+Dgp8sryOXFL9Y2uxeiq/3fb/CaV/CjBzlGKlFycuHGXya5ZyvQSJ8Q23fpO79dP7skjb2j22gW9K2ss9jpQ4U31vvtvCSy3lvyst5ddCFfiB1H6epezl7zM0jomjs+tG/1rVYVKFqpVpQjCSbdOMpQ5t/OUXjv4JwNbuHRbXcFvUtr6UlTqx4W4PElzymm0+efCmibVVPNX1CtqtapXuX3dWcpvzyecejq9BI3RJvbbezaVV6nGs7irJZcIKSVOK7hZclzy5N+deA7X4hdu/Sd166f3Y+IXbv0ndeun92avKI1m9+mLR9Ssq1HQ41lXqx4MzgopRlym8qT58PEl5yD6NKpXko0Y5lJpJLvtvCXrLB/ELt36TuvXT+7Nnt3oc25odeFxGtXqzptSjGo4cKkucXiME20+a5klknQijpWq09IjZ6TaTXDaUlKrjv3FXupZ8qTz+0ZqNgaJuDUJ1bjbNOLrW0HJN9fFNOKUE1h1McTWfB4cEx610MabrdercXusXHZKs3KWFDCz1JZXUlhLzHU7G2XY7Nozo2NWc+OfHKc8ZfJJLksYSX2sbdGFUtSvb++m3qdzUnUTeXUlKUk88/ldXMzdsbdv8Ac9bsGlKDq4bxKShyXXjPX5kWG3j0TaHuau7iVapRqSXd9j4cTfzmmvleF981endCWnaZVhWsdcuYVaclKMkqfJr9Xmu8138l2mB03RhtGez7RUbiUZVpyc6ko81xPCSTfNpRjH05ffP26Stwvben161OeKnDwU/+ZU7mPnxzl+qdTBNJcT54OX33sq03pCnT1C9rQp05OXDT4VmTWE3xRfUs4/vMx9VUqMZTaUItt8kl32+r0lp9HudN2BaWtncN9m7Gm4wSbcnKMakubS51KkUkst5SSZqdF6F9v6RXpXFO9uJulNTUJuDi5ReVnEE+vD9B1W59o2uvyjOd3VpVFCVNypuPdUpyjKcHxRaw+BejK6mavKVG8sLujf04VbWopU5xUoyXU4yWU/UZBi6ZZW+m0adGzhinThGEV14jFJLr8iMowoAABrNS0HS9Uz8PsoTfhaxL1rD+02YA4HUeibb92v5Nc3VF+GFecv8AEcjk9V6FNTjz0rdc34I1eNf90JP+EmoFyKz6l0YdIFlzo5rR586VfweSbi/UmcpqOn7p0zP4Rt7unjvyVRL19TLhYXgDSfJrkXZFKvwjffp1T68vePwlf/p1T68veW71PaW39UfFfaHbzl86VOPF60ssxYbC2nDq25bemlF/vRreeCp34Svv06p9eXvH4Svv06p9eXvLd09obap/J27aL9hS/wAp9u6G2tCj2S7t7WhD58o06az58Im08FQ1qN++Svav15e8yab1yr+Sdy/N2R/uLNXXSFsSzfd6xQbXzE6n2wi0ai86atnWy/k9StU8lOlj/EcUNvwQTS0neFbnS0++kvCoVn/6NjR2dv6us09Mu8eVuP8AFJEm3PT3o8c/BdHuJeDicIfucjTXXT7ey/ou36cf79WU/wB0Yl78HMUujvpGq/JsKq89xTj9jqZNjb9FG/6yzO4jDySuG/4cni76cN11s9gpW9NPqxTcmvrSa+w01z0qb1uPla5JeSMKcf3RGKOpt+h3edT8trtKP7WtL90TOpdDWtR/pu8Yw83ZJfxTiRfdbt3JeZ+E6/dST7zrVMepPH2GquLivdPNzWlN+GTcn9pdaJhl0e6NZL+dekpLzTiufpqsxKuk9Hli8XfSDdzfgpOUvtjTkvtIw0zSNS1V8OmWFSq/6kHLHnwuXpO10fod3hqOHWtadBPv1ZpPHmhxNelImJ6NjLUOjOx+TqOq3Hk4+H/IYtXduxqOfg23byp4OyXlSHr4Js7HSegSzp4esaxOb+bSioL1yy/sR2ukdGO0NK50dEpzl86rmr1eSbaXoSJmCGdP3T8NaW3dhKo/BKpdXXr7rGPOdTp2kdIeoc46FYWcX1SnTi2v1czfrRNdChSt0o0aUYxXeikl6ke8LwE2MOA0nZG4sfzrvKon821o0aC6vCoc/UjobPaOl235SVas/DXrVa6+rOTivQjfgmVeacFTSUVhLvHoAgAAAAAAAAAAAAAB4nThUTU4pp9afUz2AOH3H0W7U1vLem9hqPP4yh+LeX33Fdy/SiL9x9B2tWWZaHdRuIc+4l+KqeRLLcZP0osQfGsmpysTCleqaXf6RN09Ts50qi/NnFxePCs9a8qMMunqekWGrQdPU7WFWm/zZxUl9vU/KjRaX0cbR0t5ttDpuWc8U06rXf5cbeDW6qr6Xo+p6s8aXp9Wq84/FwlPD8uFyO20joa3dqOHXt6dCL/4s+ePNBN58jx6CzFOhTpJKnFKK6opJJehH6rkTcQvpHQLZUues6vUqf1aUVTXrk5N+pHc6N0abQ0n8jolOcvnVfxzz+vlL0JHXgzbR+dKhSpJKlTUYrqSWF6kelFLqR6BAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/2Q=="
                                     class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{!! Auth::user()->name !!}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHEhUSBwgVFRAWFxoaGRMXGRoWGxkWFxcWGBYeHyEZHyogHRolHRkVIjEhJSorLi4uFx81ODUsNygtLisBCgoKDg0OFRAQFysdHR0tLS0tLSstLS0tLS0tLS0tLS0tLS0rLS0tLS0tNy0tLS0tLS0tLSstNzctLS03LSstN//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABwgEBQYCAQP/xABPEAACAQMCAgIMCAkICwAAAAAAAQIDBBEFBhIhBzETFiJBUVZhcYGRk9EUFzJSVZKh0xUjM0JUgoOxsiQlNGJjcrPSCERFRlNzlKKjwdT/xAAXAQEBAQEAAAAAAAAAAAAAAAAAAQID/8QAHBEBAQACAwEBAAAAAAAAAAAAAAEREgIhUUFh/9oADAMBAAIRAxEAPwCcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD43gDE1PU7LSoOpqV1ClTXXOclFZfUsvvmn7fdpeMdt7WPvIh/0h9wq7uaVlQn3NGPHUX9pUXcrzqHP9oRdpFnbXs3G81GFCCTfHOM5JtY5Ygm8v1cjc45iLX9vu0vGO29rH3jt92l4x23tY+8rT2u6H442/srj7sdr2ieONv7K4+7GsMrQadurQtVk4aXqtGrKMeKShNSxFNJt46llrn5TFe/NpxbUtw23L+1j7yFfgtvs3RK1Wzv41quoTVGnVhGUMUYOXZPlpPvTWcfnIjG2pdnlGLqRjxSS4pPEY5eMtrqS62JxFuO33aXjHbe1j7x2+7S8Y7b2sfeVt7TLfxx0z2tb7k+9pluv98dM9rW+5Jieiz1luXRtQpzq2WqUZ0qfy6kZpxh3+b6kYfb7tLxjtvax95DG97eOy9GttNjWhKvdTdetOGcSgmnDHEk8fkkm0vkMjLTbRX9WNOV1TpKWfxlVuMI4TeZNJtdWOrvos4wWz7fdpeMdt7WPvHb7tLxjtvax95W7tMoeOOme1rfcnqlsinVajS3dprk2kkqtZtt9SX4nrJieizlHcmkVqLuKWpUnbptOtxpQyutcT5Z8hhLfm0/GK19rD3kA9KdSGk/BtJtaicLOknUxnErmr3c35eTWPBxM53aO1NT3dWdDSVDjjBzcptxioppc2k+bbXIusFoe3zafjHa+1h7x2+bT8Y7X2sPeQZPoR3XTTc61qkllt1JYSXX+YRrJY6nnyjWX6q5ul69pmrpy0u/pVYx63TmpY8+Ooxe3LbXf1629rD3kCbInU2zpV/qKlw1K2LWh3suTzUkvDhNteWmyP9Ps62oVYUbWOalScYRX9aTSXo5jVFu+3LbP09be1h7x25bZ+nrb2sPeQmugnX+/qlr/AOX/ACEcbg0qWiXFS3qXEKkqUuFzhnhcklnGUnyfL0MawW90/cGkam+HTtTo1ZfNhUjJ48yeTGv93bd02pKlqGtUKdWOOKE6kYyWUmspvlyafpIN/wBH/QZX99K6lHubaDw+86lVSgl5e54/Wjotc6E9U1m4q3N1uGnx1Zym12KTxl8ku66ksJeYlklEk9vm0/GO19rD3jt82n4x2vtYe8qbq1rCwrVaNKspqnOUOyJYUuGTjxJZfJ45HZ9H/Rhe71ozr07+NGnGfAuKDlxNJOWMNcllL1l1mBYGnvja9VqNHcFtKUmkoqrFttvCSS7+ToIviIe2j0LVNDvKNze6rCrClLi7GoOLcknwPLb6pYfoJhisGbhX0AEAxtSuqNlSnVuZ4p04ucn4IxWX9iMk1+v6Tb69b1ba7nJU6seGTg8SSfgbTXrTAp/uDVaut3Na5uH3VWblh95N9yvQsL0GVp9nt2rTT1DW61Oq85hC2VVLm8d060c8sPqXWTY+gbbr/wBp3frp/dj4hdu/Sd166f3Z02iIb/B20vGS5/6KP/0mTpugba1OrTo2e4LmVSpOMIr4FH5Unhf6zyXlJb+IXbv0ndeun92bbavRJom2bmF1a3VedSClwqo4OKcouOe5iuaTZNhEPTDf0fhVOxsH/J7GlGjHyzwnN8v1V54sx+j3o6vt7xqzt7yNKFJxjxSi5cUpJtpYfeWPrIlW86D9Cvak6txqt25zlKUnmnzlJuTf5PwtncbN2zZ7St1bafOcoKUpOU8cTlJ5beEl4F1dSQ266EQfEDqP09S9nL3mRp3QPc29anO81inOlGcXOCpyTlBSTlHm8c1leknQx76g7qnOEasoOcZR4444o8SaysprKzlZT6ibUVU6Ute/D+pVqlKWaVN9ip+Dgp8sryOXFL9Y2uxeiq/3fb/CaV/CjBzlGKlFycuHGXya5ZyvQSJ8Q23fpO79dP7skjb2j22gW9K2ss9jpQ4U31vvtvCSy3lvyst5ddCFfiB1H6epezl7zM0jomjs+tG/1rVYVKFqpVpQjCSbdOMpQ5t/OUXjv4JwNbuHRbXcFvUtr6UlTqx4W4PElzymm0+efCmibVVPNX1CtqtapXuX3dWcpvzyecejq9BI3RJvbbezaVV6nGs7irJZcIKSVOK7hZclzy5N+deA7X4hdu/Sd166f3Y+IXbv0ndeun92avKI1m9+mLR9Ssq1HQ41lXqx4MzgopRlym8qT58PEl5yD6NKpXko0Y5lJpJLvtvCXrLB/ELt36TuvXT+7Nnt3oc25odeFxGtXqzptSjGo4cKkucXiME20+a5klknQijpWq09IjZ6TaTXDaUlKrjv3FXupZ8qTz+0ZqNgaJuDUJ1bjbNOLrW0HJN9fFNOKUE1h1McTWfB4cEx610MabrdercXusXHZKs3KWFDCz1JZXUlhLzHU7G2XY7Nozo2NWc+OfHKc8ZfJJLksYSX2sbdGFUtSvb++m3qdzUnUTeXUlKUk88/ldXMzdsbdv8Ac9bsGlKDq4bxKShyXXjPX5kWG3j0TaHuau7iVapRqSXd9j4cTfzmmvleF981endCWnaZVhWsdcuYVaclKMkqfJr9Xmu8138l2mB03RhtGez7RUbiUZVpyc6ko81xPCSTfNpRjH05ffP26Stwvben161OeKnDwU/+ZU7mPnxzl+qdTBNJcT54OX33sq03pCnT1C9rQp05OXDT4VmTWE3xRfUs4/vMx9VUqMZTaUItt8kl32+r0lp9HudN2BaWtncN9m7Gm4wSbcnKMakubS51KkUkst5SSZqdF6F9v6RXpXFO9uJulNTUJuDi5ReVnEE+vD9B1W59o2uvyjOd3VpVFCVNypuPdUpyjKcHxRaw+BejK6mavKVG8sLujf04VbWopU5xUoyXU4yWU/UZBi6ZZW+m0adGzhinThGEV14jFJLr8iMowoAABrNS0HS9Uz8PsoTfhaxL1rD+02YA4HUeibb92v5Nc3VF+GFecv8AEcjk9V6FNTjz0rdc34I1eNf90JP+EmoFyKz6l0YdIFlzo5rR586VfweSbi/UmcpqOn7p0zP4Rt7unjvyVRL19TLhYXgDSfJrkXZFKvwjffp1T68vePwlf/p1T68veW71PaW39UfFfaHbzl86VOPF60ssxYbC2nDq25bemlF/vRreeCp34Svv06p9eXvH4Svv06p9eXvLd09obap/J27aL9hS/wAp9u6G2tCj2S7t7WhD58o06az58Im08FQ1qN++Svav15e8yab1yr+Sdy/N2R/uLNXXSFsSzfd6xQbXzE6n2wi0ai86atnWy/k9StU8lOlj/EcUNvwQTS0neFbnS0++kvCoVn/6NjR2dv6us09Mu8eVuP8AFJEm3PT3o8c/BdHuJeDicIfucjTXXT7ey/ou36cf79WU/wB0Yl78HMUujvpGq/JsKq89xTj9jqZNjb9FG/6yzO4jDySuG/4cni76cN11s9gpW9NPqxTcmvrSa+w01z0qb1uPla5JeSMKcf3RGKOpt+h3edT8trtKP7WtL90TOpdDWtR/pu8Yw83ZJfxTiRfdbt3JeZ+E6/dST7zrVMepPH2GquLivdPNzWlN+GTcn9pdaJhl0e6NZL+dekpLzTiufpqsxKuk9Hli8XfSDdzfgpOUvtjTkvtIw0zSNS1V8OmWFSq/6kHLHnwuXpO10fod3hqOHWtadBPv1ZpPHmhxNelImJ6NjLUOjOx+TqOq3Hk4+H/IYtXduxqOfg23byp4OyXlSHr4Js7HSegSzp4esaxOb+bSioL1yy/sR2ukdGO0NK50dEpzl86rmr1eSbaXoSJmCGdP3T8NaW3dhKo/BKpdXXr7rGPOdTp2kdIeoc46FYWcX1SnTi2v1czfrRNdChSt0o0aUYxXeikl6ke8LwE2MOA0nZG4sfzrvKon821o0aC6vCoc/UjobPaOl235SVas/DXrVa6+rOTivQjfgmVeacFTSUVhLvHoAgAAAAAAAAAAAAAB4nThUTU4pp9afUz2AOH3H0W7U1vLem9hqPP4yh+LeX33Fdy/SiL9x9B2tWWZaHdRuIc+4l+KqeRLLcZP0osQfGsmpysTCleqaXf6RN09Ts50qi/NnFxePCs9a8qMMunqekWGrQdPU7WFWm/zZxUl9vU/KjRaX0cbR0t5ttDpuWc8U06rXf5cbeDW6qr6Xo+p6s8aXp9Wq84/FwlPD8uFyO20joa3dqOHXt6dCL/4s+ePNBN58jx6CzFOhTpJKnFKK6opJJehH6rkTcQvpHQLZUues6vUqf1aUVTXrk5N+pHc6N0abQ0n8jolOcvnVfxzz+vlL0JHXgzbR+dKhSpJKlTUYrqSWF6kelFLqR6BAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/2Q=="
                                         class="img-circle" alt="User Image"/>
                                    <p>
                                        {!! Auth::user()->name !!}
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <strong>Copyright © {{date('Y')}} <a href="#">Effort</a>.</strong> All rights reserved.
        </footer>

    </div>
@else
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{!! url('/') !!}">
                    Effort Play
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{!! url('/home') !!}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a href="{!! url('/login') !!}">Login</a></li>
                    <li><a href="{!! url('/register') !!}">Register</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    @endif

    <!-- jQuery 3.1.1 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    {{ Html::script('/admin.js') }}
    {{ Html::script('/jscolor.js') }}


    @yield('scripts')
</body>
</html>