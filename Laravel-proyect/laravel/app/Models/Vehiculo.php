<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

/**
 * @SWG\Definition(
 *      definition="Vehiculo",
 *      required={"marcas_id", "created_at"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="placa",
 *          description="placa",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="color",
 *          description="color",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tipo",
 *          description="tipo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="marcas_id",
 *          description="marcas_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Vehiculo extends Model
{
    use SoftDeletes;

    public $table = 'vehiculos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'placa',
        'color',
        'tipo',
        'marcas_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'placa' => 'string',
        'color' => 'string',
        'tipo' => 'string',
        'marcas_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function marcas()
    {
        return $this->belongsTo(\App\Models\Marca::class, 'marcas_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function personaPorVehiculos()
    {
        return $this->hasMany(\App\Models\PersonaPorVehiculo::class, 'vehiculos_id');
    }


    public function getVehiculosPersonas($id,$vehiculo_id)
    {
        return DB::table('persona_por_vehiculo')
            ->join('vehiculos', 'vehiculos.id', '=', 'persona_por_vehiculo.vehiculos_id')
            ->join('personas', 'personas.id', '=', 'persona_por_vehiculo.personas_id')
            ->where([
                ['personas.rols_id', '=', $id],
                ['persona_por_vehiculo.vehiculos_id','=',$vehiculo_id]
            ])
            ->select('personas.*')
            ->get();
    }

    public function getVehiculos()
    {
        return DB::table('vehiculos')
            ->join('marcas', 'marcas.id', '=', 'vehiculos.marcas_id')
            ->whereNull('vehiculos.deleted_at')
            ->select('vehiculos.*','marcas.nombre as marca')
            ->get();
    }

    public function linkUser($persona,$vehiculo){
        $id = DB::table('persona_por_vehiculo')->insertGetId(
            array(
                'vehiculos_id' =>$vehiculo, 'personas_id' => $persona,
                'created_at'=> \Carbon\Carbon::now()
            )
        );
        return $id;
    }
}
