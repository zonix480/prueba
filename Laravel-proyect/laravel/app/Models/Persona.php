<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
/**
 * @SWG\Definition(
 *      definition="Persona",
 *      required={"cedula", "primer_nombre", "ciudad_id", "rols_id", "created_at"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cedula",
 *          description="cedula",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="primer_nombre",
 *          description="primer_nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="segundo_nombre",
 *          description="segundo_nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="apellidos",
 *          description="apellidos",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="direccion",
 *          description="direccion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telefono",
 *          description="telefono",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ciudad_id",
 *          description="ciudad_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="rols_id",
 *          description="rols_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="contraseña",
 *          description="contraseña",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Persona extends Model
{
    use SoftDeletes;

    public $table = 'personas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'cedula',
        'primer_nombre',
        'segundo_nombre',
        'apellidos',
        'direccion',
        'telefono',
        'ciudad_id',
        'remember_token',
        'rols_id',
        'contraseña'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cedula' => 'string',
        'primer_nombre' => 'string',
        'segundo_nombre' => 'string',
        'apellidos' => 'string',
        'direccion' => 'string',
        'telefono' => 'string',
        'ciudad_id' => 'integer',
        'remember_token' => 'string',
        'rols_id' => 'integer',
        'contraseña' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cedula' => 'required',
        'primer_nombre' => 'required',
        'ciudad_id' => 'required',
        'rols_id' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ciudad()
    {
        return $this->belongsTo(\App\Models\Ciudade::class, 'ciudad_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function rols()
    {
        return $this->belongsTo(\App\Models\Rol::class, 'rols_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function personaPorVehiculos()
    {
        return $this->hasMany(\App\Models\PersonaPorVehiculo::class, 'personas_id');
    }

    public function getPerson()
    {
        return DB::table('personas')
        ->join('ciudades', 'ciudades.id', '=', 'personas.ciudad_id')
        ->join('persona_por_vehiculo', 'persona_por_vehiculo.personas_id', '=', 'personas.id')
        ->join('vehiculos', 'vehiculos.id', '=', 'persona_por_vehiculo.vehiculos_id')
        ->join('rols', 'rols.id', '=', 'personas.rols_id')
            ->whereNull('personas.deleted_at')
            ->select('personas.*','ciudades.nombre as ciudad','vehiculos.*','rols.nombre as rol')
            ->get();
    }

    public function createPersona($input){
            $id = DB::table('personas')->insertGetId(
                array(
                    'cedula' => $input['cedula'], 'email' => $input['email'], 'primer_nombre' => $input['primer_nombre'],
                    'segundo_nombre' => $input['segundo_nombre'], 'apellidos' => $input['apellidos'],
                    'direccion' => $input['direccion'], 'telefono' => $input['telefono'],
                    'rols_id' => $input['rols_id'],
                    'ciudad_id' => $input['ciudad_id'],
                    'contraseña' => Hash::make('123456789'),
                    'created_at'=> \Carbon\Carbon::now()
                )
            );
            return $id;
    }


}
