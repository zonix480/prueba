<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePersona_por_VehiculoAPIRequest;
use App\Http\Requests\API\UpdatePersona_por_VehiculoAPIRequest;
use App\Models\Persona_por_Vehiculo;
use App\Repositories\Persona_por_VehiculoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Persona_por_VehiculoController
 * @package App\Http\Controllers\API
 */

class Persona_por_VehiculoAPIController extends AppBaseController
{
    /** @var  Persona_por_VehiculoRepository */
    private $personaPorVehiculoRepository;

    public function __construct(Persona_por_VehiculoRepository $personaPorVehiculoRepo)
    {
        $this->personaPorVehiculoRepository = $personaPorVehiculoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/personaPorVehiculos",
     *      summary="Get a listing of the Persona_por_Vehiculos.",
     *      tags={"Persona_por_Vehiculo"},
     *      description="Get all Persona_por_Vehiculos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Persona_por_Vehiculo")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $personaPorVehiculos = $this->personaPorVehiculoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($personaPorVehiculos->toArray(), 'Persona Por  Vehiculos retrieved successfully');
    }

    /**
     * @param CreatePersona_por_VehiculoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/personaPorVehiculos",
     *      summary="Store a newly created Persona_por_Vehiculo in storage",
     *      tags={"Persona_por_Vehiculo"},
     *      description="Store Persona_por_Vehiculo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Persona_por_Vehiculo that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Persona_por_Vehiculo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Persona_por_Vehiculo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePersona_por_VehiculoAPIRequest $request)
    {
        $input = $request->all();

        $personaPorVehiculo = $this->personaPorVehiculoRepository->create($input);

        return $this->sendResponse($personaPorVehiculo->toArray(), 'Persona Por  Vehiculo saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/personaPorVehiculos/{id}",
     *      summary="Display the specified Persona_por_Vehiculo",
     *      tags={"Persona_por_Vehiculo"},
     *      description="Get Persona_por_Vehiculo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Persona_por_Vehiculo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Persona_por_Vehiculo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Persona_por_Vehiculo $personaPorVehiculo */
        $personaPorVehiculo = $this->personaPorVehiculoRepository->find($id);

        if (empty($personaPorVehiculo)) {
            return $this->sendError('Persona Por  Vehiculo not found');
        }

        return $this->sendResponse($personaPorVehiculo->toArray(), 'Persona Por  Vehiculo retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePersona_por_VehiculoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/personaPorVehiculos/{id}",
     *      summary="Update the specified Persona_por_Vehiculo in storage",
     *      tags={"Persona_por_Vehiculo"},
     *      description="Update Persona_por_Vehiculo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Persona_por_Vehiculo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Persona_por_Vehiculo that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Persona_por_Vehiculo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Persona_por_Vehiculo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePersona_por_VehiculoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Persona_por_Vehiculo $personaPorVehiculo */
        $personaPorVehiculo = $this->personaPorVehiculoRepository->find($id);

        if (empty($personaPorVehiculo)) {
            return $this->sendError('Persona Por  Vehiculo not found');
        }

        $personaPorVehiculo = $this->personaPorVehiculoRepository->update($input, $id);

        return $this->sendResponse($personaPorVehiculo->toArray(), 'Persona_por_Vehiculo updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/personaPorVehiculos/{id}",
     *      summary="Remove the specified Persona_por_Vehiculo from storage",
     *      tags={"Persona_por_Vehiculo"},
     *      description="Delete Persona_por_Vehiculo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Persona_por_Vehiculo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Persona_por_Vehiculo $personaPorVehiculo */
        $personaPorVehiculo = $this->personaPorVehiculoRepository->find($id);

        if (empty($personaPorVehiculo)) {
            return $this->sendError('Persona Por  Vehiculo not found');
        }

        $personaPorVehiculo->delete();

        return $this->sendSuccess('Persona Por  Vehiculo deleted successfully');
    }
}
