<?php

namespace App\Http\Controllers;

use App\DataTables\LayerDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLayerRequest;
use App\Http\Requests\UpdateLayerRequest;
use App\Repositories\LayerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class LayerController extends AppBaseController
{
    /** @var  LayerRepository */
    private $layerRepository;

    public function __construct(LayerRepository $layerRepo)
    {
        $this->layerRepository = $layerRepo;
    }

    /**
     * Display a listing of the Layer.
     *
     * @param LayerDataTable $layerDataTable
     * @return Response
     */
    public function index(LayerDataTable $layerDataTable)
    {
        return $layerDataTable->render('layers.index');
    }

    /**
     * Show the form for creating a new Layer.
     *
     * @return Response
     */
    public function create()
    {
        return view('layers.create');
    }

    /**
     * Store a newly created Layer in storage.
     *
     * @param CreateLayerRequest $request
     *
     * @return Response
     */
    public function store(CreateLayerRequest $request)
    {
        $input = $request->all();

        $layer = $this->layerRepository->create($input);

        Flash::success('Layer saved successfully.');

        return redirect(route('layers.index'));
    }

    /**
     * Display the specified Layer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $layer = $this->layerRepository->find($id);

        if (empty($layer)) {
            Flash::error('Layer not found');

            return redirect(route('layers.index'));
        }

        return view('layers.show')->with('layer', $layer);
    }

    /**
     * Show the form for editing the specified Layer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $layer = $this->layerRepository->find($id);

        if (empty($layer)) {
            Flash::error('Layer not found');

            return redirect(route('layers.index'));
        }

        return view('layers.edit')->with('layer', $layer);
    }

    /**
     * Update the specified Layer in storage.
     *
     * @param  int              $id
     * @param UpdateLayerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLayerRequest $request)
    {
        $layer = $this->layerRepository->find($id);

        if (empty($layer)) {
            Flash::error('Layer not found');

            return redirect(route('layers.index'));
        }

        $layer = $this->layerRepository->update($request->all(), $id);

        Flash::success('Layer updated successfully.');

        return redirect(route('layers.index'));
    }

    /**
     * Remove the specified Layer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $layer = $this->layerRepository->find($id);

        if (empty($layer)) {
            Flash::error('Layer not found');

            return redirect(route('layers.index'));
        }

        $this->layerRepository->delete($id);

        Flash::success('Layer deleted successfully.');

        return redirect(route('layers.index'));
    }
}
