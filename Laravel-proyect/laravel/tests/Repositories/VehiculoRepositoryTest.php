<?php namespace Tests\Repositories;

use App\Models\Vehiculo;
use App\Repositories\VehiculoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class VehiculoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var VehiculoRepository
     */
    protected $vehiculoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->vehiculoRepo = \App::make(VehiculoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_vehiculo()
    {
        $vehiculo = factory(Vehiculo::class)->make()->toArray();

        $createdVehiculo = $this->vehiculoRepo->create($vehiculo);

        $createdVehiculo = $createdVehiculo->toArray();
        $this->assertArrayHasKey('id', $createdVehiculo);
        $this->assertNotNull($createdVehiculo['id'], 'Created Vehiculo must have id specified');
        $this->assertNotNull(Vehiculo::find($createdVehiculo['id']), 'Vehiculo with given id must be in DB');
        $this->assertModelData($vehiculo, $createdVehiculo);
    }

    /**
     * @test read
     */
    public function test_read_vehiculo()
    {
        $vehiculo = factory(Vehiculo::class)->create();

        $dbVehiculo = $this->vehiculoRepo->find($vehiculo->id);

        $dbVehiculo = $dbVehiculo->toArray();
        $this->assertModelData($vehiculo->toArray(), $dbVehiculo);
    }

    /**
     * @test update
     */
    public function test_update_vehiculo()
    {
        $vehiculo = factory(Vehiculo::class)->create();
        $fakeVehiculo = factory(Vehiculo::class)->make()->toArray();

        $updatedVehiculo = $this->vehiculoRepo->update($fakeVehiculo, $vehiculo->id);

        $this->assertModelData($fakeVehiculo, $updatedVehiculo->toArray());
        $dbVehiculo = $this->vehiculoRepo->find($vehiculo->id);
        $this->assertModelData($fakeVehiculo, $dbVehiculo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_vehiculo()
    {
        $vehiculo = factory(Vehiculo::class)->create();

        $resp = $this->vehiculoRepo->delete($vehiculo->id);

        $this->assertTrue($resp);
        $this->assertNull(Vehiculo::find($vehiculo->id), 'Vehiculo should not exist in DB');
    }
}
