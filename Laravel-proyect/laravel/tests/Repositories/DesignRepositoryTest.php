<?php namespace Tests\Repositories;

use App\Models\Design;
use App\Repositories\DesignRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DesignRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DesignRepository
     */
    protected $designRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->designRepo = \App::make(DesignRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_design()
    {
        $design = factory(Design::class)->make()->toArray();

        $createdDesign = $this->designRepo->create($design);

        $createdDesign = $createdDesign->toArray();
        $this->assertArrayHasKey('id', $createdDesign);
        $this->assertNotNull($createdDesign['id'], 'Created Design must have id specified');
        $this->assertNotNull(Design::find($createdDesign['id']), 'Design with given id must be in DB');
        $this->assertModelData($design, $createdDesign);
    }

    /**
     * @test read
     */
    public function test_read_design()
    {
        $design = factory(Design::class)->create();

        $dbDesign = $this->designRepo->find($design->id);

        $dbDesign = $dbDesign->toArray();
        $this->assertModelData($design->toArray(), $dbDesign);
    }

    /**
     * @test update
     */
    public function test_update_design()
    {
        $design = factory(Design::class)->create();
        $fakeDesign = factory(Design::class)->make()->toArray();

        $updatedDesign = $this->designRepo->update($fakeDesign, $design->id);

        $this->assertModelData($fakeDesign, $updatedDesign->toArray());
        $dbDesign = $this->designRepo->find($design->id);
        $this->assertModelData($fakeDesign, $dbDesign->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_design()
    {
        $design = factory(Design::class)->create();

        $resp = $this->designRepo->delete($design->id);

        $this->assertTrue($resp);
        $this->assertNull(Design::find($design->id), 'Design should not exist in DB');
    }
}
