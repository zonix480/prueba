<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Layer;

class LayerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_layer()
    {
        $layer = factory(Layer::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/layers', $layer
        );

        $this->assertApiResponse($layer);
    }

    /**
     * @test
     */
    public function test_read_layer()
    {
        $layer = factory(Layer::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/layers/'.$layer->id
        );

        $this->assertApiResponse($layer->toArray());
    }

    /**
     * @test
     */
    public function test_update_layer()
    {
        $layer = factory(Layer::class)->create();
        $editedLayer = factory(Layer::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/layers/'.$layer->id,
            $editedLayer
        );

        $this->assertApiResponse($editedLayer);
    }

    /**
     * @test
     */
    public function test_delete_layer()
    {
        $layer = factory(Layer::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/layers/'.$layer->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/layers/'.$layer->id
        );

        $this->response->assertStatus(404);
    }
}
